#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from collections import defaultdict
from re import match as re_match
from ruamel.yaml import YAML

__all__ = ["virglrenderer_expectations_paths", "mesa_expectations_paths"]


def __build_expectations_paths(target):
    def defaultdictlist() -> defaultdict:
        return defaultdict(list)
    dct = defaultdict(defaultdictlist)
    file_name = f"./expectations_paths/{target}.yml"
    with open(file_name, 'r') as f:
        yaml = YAML()
        contents = yaml.load(f)
    for job_key, job_content in contents.items():
        if re_match(r"^\(.*\)$", job_key):
            job_key = eval(job_key)
        dct[job_key]['path'] = job_content['path']
        dct[job_key]['files'] = job_content['files']
    return dct


virglrenderer_expectations_paths = __build_expectations_paths('virglrenderer')
mesa_expectations_paths = __build_expectations_paths('mesa')
